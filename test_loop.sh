#!/bin/sh

# Oh! This might just be the infamous infinite loop!

a=4
while [ "$a" -lt 10 ]
do
   #echo `expr $a + 1`
   a=`expr $a + 1`
done

echo 'Done looping!'
